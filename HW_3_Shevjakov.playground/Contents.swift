import UIKit


/*
 1. Write a function named printFullName that takes two strings called firstName and lastName.
 The function should print out the full name defined as firstName + " " + lastName.
 Use it to print out your own full name.
 */

/*
 2. Создайте функцию, которая принимает параметры и вычисляет площадь круга.
 */

/*
 3. Создайте функцию, которая принимает параметры и вычисляет расстояние между двумя точками.
 */

/*
 4. Напишите функцию, которое считает факториал числа.
 */

/*
 5. Напишите функцию, которая вычисляет N-ое число Фибоначчи
 */

/*
 6. First, write the following function:
 
 func isNumberDivisible(_ number: Int, by divisor: Int) -> Bool
 
 You’ll use this to determine if one number is divisible by another. It should return true when number is divisible by divisor.
 
 Hint: You can use the modulo (%) operator to help you out here.
 
 Next, write the main function:
 
 func isPrime(_ number: Int) -> Bool
 
 This should return true if number is prime, and false otherwise. A number is prime if it’s only divisible by 1 and itself.
 You should loop through the numbers from 1 to the number and find the number’s divisors.
 If it has any divisors other than 1 and itself, then the number isn’t prime. You’ll need to use the isNumberDivisible(_:by:) function you wrote earlier.
 Use this function to check the following cases:
 
 isPrime(6) // false
 isPrime(13) // true
 isPrime(8893) // true
 
 Hint 1: Numbers less than 0 should not be considered prime. Check for this case at the start of the function and return early if the number is less than 0.
 Hint 2: Use a for loop to find divisors. If you start at 2 and end before the number itself, then as soon as you find a divisor, you can return false.
 */


